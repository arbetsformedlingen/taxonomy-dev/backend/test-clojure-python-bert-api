(ns se.jobtechdev.cpb.sbert
  (:require [clojure.string :as string]
            [clojure.math :as cm]
            [python.sentence_transformers :refer [SentenceTransformer]]
            [libpython-clj2.python :refer [py. initialize! run-simple-string]])
  (:gen-class))

(defn init-sbert! []
  (initialize! #_{:python-verbose true})
  #_(require '[sentence_transformers]))

(defn distance [v0 v1]
  (apply + (map (fn [x y] (let [z (- x y)] (* z  z))) v0 v1)))

(defn norm [v]
  (cm/sqrt (apply + (mapv #(cm/pow % 2.0) v))))

(defn normalize [v]
  (let [norm (norm v)]
    (mapv #(/ % norm) v)))

(defn distance-from [k0 k1 m]
  [k1 (distance (k0 m) (k1 m))])

(defn all-distances-from [k m]
  [k (->> (map (fn [d] (distance-from k d m)) (keys m)) (into {}))])

(defn all-distances [things]
  (->> (map #(all-distances-from % things) (keys things))
       (into {})))

(defn word-encoding [word]
  {(keyword word) (first (mapv normalize (py. (SentenceTransformer "KBLab/sentence-bert-swedish-cased") "encode" [word])))})

(defn word-encodings [words]
  (->> (string/split words #"[ ,]")
       (remove string/blank?)
       ((partial map word-encoding))
       (into {})
       all-distances))

(comment
  (initialize!)
  (run-simple-string "import sys; print(sys.version_info)")
  (run-simple-string "import os; print(os.environ)")

  (word-encodings "ärtor bönor motorsåg")
  (word-encodings "tall tal tull"))
