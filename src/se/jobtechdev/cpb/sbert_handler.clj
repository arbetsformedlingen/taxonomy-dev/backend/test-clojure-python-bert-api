(ns se.jobtechdev.cpb.sbert-handler
  (:require [se.jobtechdev.cpb.sbert :as sbert]))

(defn handler [{{{:keys [text]} :query} :parameters}]
  (sbert/init-sbert!)
  {:status 200 :body (sbert/word-encodings text)})
