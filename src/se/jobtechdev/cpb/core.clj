(ns se.jobtechdev.cpb.core
  (:require
   [nrepl.server :refer [start-server #_stop-server]]
   [reitit.ring :as ring]
   [reitit.coercion.spec]
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [malli.util :as mu]
   [reitit.ring.coercion :as coercion]
   reitit.coercion.malli
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [ring.middleware.reload :refer [wrap-reload]]
   [reitit.ring.middleware.exception :as exception]
   [reitit.ring.middleware.parameters :as parameters]
   [reitit.ring.middleware.multipart :as multipart]
   [ring.adapter.jetty :as jetty]
   [muuntaja.core :as m]
   [se.jobtechdev.cpb.sbert-handler :as sbert])
  (:gen-class))

(defn ^:private base-app [_config]
  (ring/ring-handler
   (ring/router
    [["/swagger.json"
      {:get {:no-doc true
             :swagger {:info  {:title "Clojure Python BERT API"
                               :description "Demonstrates how to serve a BERT model using Clojure."}
                       :tags [{:name "swagger" :description "Functionality demonstration."}]}
             :handler (swagger/create-swagger-handler)}}]
     ["/sbert"
      {:get {:summary "SBERT query runner!"
             :parameters {:query [:map [:text string?]]}
             :responses {200 {:body [:map {:closed false}]}}
             :handler sbert/handler}}]]
    {:data {:coercion (reitit.coercion.malli/create
                       {;; set of keys to include in error messages
                        :error-keys #{:type :coercion :in :schema :value :errors :humanized :transformed}
                           ;; schema identity function (default: close all map schemas)
                        :compile mu/closed-schema
                           ;; strip-extra-keys (effects only predefined transformers)
                        :strip-extra-keys true
                           ;; add/set default values
                        :default-values true
                           ;; malli options
                        :options nil})
            :muuntaja m/instance
            :middleware [;; swagger feature
                         swagger/swagger-feature
                           ;; query-params & form-params
                         parameters/parameters-middleware
                           ;; content-negotiation 
                         muuntaja/format-negotiate-middleware
                           ;; encoding response body
                         muuntaja/format-response-middleware
                           ;; exception handling
                         (exception/create-exception-middleware
                          {::exception/default (partial exception/wrap-log-to-console exception/default-handler)})
                           ;; decoding request body
                         muuntaja/format-request-middleware
                           ;; coercing response bodys
                         coercion/coerce-response-middleware
                           ;; coercing request parameters
                         coercion/coerce-request-middleware
                           ;; multipart
                         multipart/multipart-middleware]}})
   (ring/routes
    (swagger-ui/create-swagger-ui-handler
     {:path "/"
      :config {:validatorUrl nil :operationsSorter "alpha"}})
    (ring/create-default-handler))))

(defn create-application [& {:keys [app] :or {app {}}}]
  (let [config {:repl {:port 7888} :jetty {:port 3333 :join? false} :app app}
        configured-app (base-app (config :app))
        repl-server (start-server (config :repl))
        jetty-server (jetty/run-jetty configured-app (config :jetty))]
    {:repl-server repl-server
     :app app
     :jetty-server jetty-server}))

(defn -main
  "Launch the jetty server for our application."
  [& args]
  (create-application args))

(comment
  (defonce server
    (jetty/run-jetty (base-app {}) {:port 3000 :join? false}))
  (do
    (.stop server)
    (.start server)))