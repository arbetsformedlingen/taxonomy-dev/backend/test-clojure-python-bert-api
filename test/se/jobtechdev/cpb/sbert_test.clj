(ns se.jobtechdev.cpb.sbert-test
  (:require [clojure.test :refer [deftest is testing]]
            [se.jobtechdev.cpb.sbert-handler :as sbert])
  (:gen-class))

(deftest handler-test
  (testing "Test the handler with text"
    (let [result (sbert/handler {:parameters {:query {:text "kör mitt,test"}}})]
      (is (= 200 (get-in result [:status])))
      (is (< 0.26 (get-in result [:body :kör :mitt])))
      #_(is (= {:status 200 :body {:kör {:kör 0.0 :mitt 0.264144095234421 :test 1.051313639168957}
                                   :mitt {:kör 0.264144095234421 :mitt 0.0 :test 1.103531068716288}
                                   :test {:kör 1.051313639168957 :mitt 1.103531068716288 :test 0.0}}} result)))))