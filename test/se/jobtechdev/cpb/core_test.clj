(ns se.jobtechdev.cpb.core-test
  (:require [clojure.data.json :as json]
            [clojure.test :refer [deftest is testing]]
            [ring.mock.request :refer [request]]
            [se.jobtechdev.cpb.core :as c])
  (:gen-class))

(deftest app-sbert-test
  (let [response ((#'c/base-app {}) (request :get "/sbert" {:text "min text"}))
        response-status (response :status)
        response-body (json/read-str (slurp (response :body)) :key-fn keyword)]
    (testing "Test that we can talk to the SBERT API"
      (is (= 200 response-status))
      (is (= 0.0 (get-in response-body [:min :min])))
      (is (= (get-in response-body [:min :text]) (get-in response-body [:text :min])))
      (is (<  0.9464 (get-in response-body [:min :text])))
      #_(is (= {:min {:min 0.0
                      :text 0.9464398597024833}
                :text {:text 0.0
                       :min 0.9464398597024833}} response-body)))))