(ns se.jobtechdev.cpb.python-test
  (:require [clojure.test :refer [deftest testing is]]
            [se.jobtechdev.cpb.sbert :as sbert]))

#_(py/initialize!
   #_{:python-home "/home/johan"})

#_(deftest sbert-test
    (testing "Check that our SBERT-thing responds."
      (sbert/init-sbert!)
      (let [result (sbert/word-encodings "kaka,tårta chimär")]

        (is (= 0.0 (get-in result [:kaka :kaka])))
        (is (< (get-in result [:kaka :tårta])
               (get-in result [:kaka :chimär]))))))