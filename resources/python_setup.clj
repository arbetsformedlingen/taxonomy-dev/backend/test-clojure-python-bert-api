(ns python-setup
  (:require
   [libpython-clj2.python :as py]
   [libpython-clj2.require :as rp]
   [libpython-clj2.codegen :as codegen]))

(defn gen-code! []
  (py/initialize!)
  (rp/require-python '[sentence_transformers])
  (codegen/write-namespace!
   "builtins"
   {:output-dir "generated"
    :symbol-name-remaps {"AssertionError" "PyAssertionError"
                         "Exception" "PyException"}})
  (codegen/write-namespace!
   "sentence_transformers"
   {:output-dir "generated"
    :symbol-name-remaps {"AssertionError" "PyAssertionError"
                         "Exception" "PyException"}}))
#_(defonce py (init!))