FROM ubuntu:22.04

ARG CLOJURE_TOOLS_VERSION=1.11.1.1182

RUN apt-get -qq update \
   && apt install -y software-properties-common \
   && add-apt-repository -y ppa:deadsnakes/ppa \
	&& apt-get -qq update \
	&& apt-get -qq -y install strace git curl rlwrap wget bzip2 openjdk-17-jdk-headless \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata \
	&& ln -fs /usr/share/zoneinfo/Europe/Stockholm /etc/localtime \
	&& dpkg-reconfigure --frontend noninteractive tzdata \
	&& apt-get -qq -y install python3 python-is-python3 python3-pip python3-distutils \
#	&& apt-get -qq -y install python3.9 python-is-python3 libpython3.9 python3-pip python3-distutils \
#	&& apt-get -qq -y install python3-numpy python3-torch python3-sklearn python3-sklearn-lib \
   && curl -o install-clojure https://download.clojure.org/install/linux-install-${CLOJURE_TOOLS_VERSION}.sh \
   && chmod +x install-clojure \
   && ./install-clojure && rm install-clojure \
   && apt-get -qq -y autoremove \
   && apt-get autoclean \
   && rm -rf /var/lib/apt/lists/* /var/log/dpkg.log \
   && pip3 install numpy \
   && pip3 install torch --extra-index-url https://download.pytorch.org/whl/cpu

# Setup app
RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app

RUN python3 -mpip install -r /app/requirements.txt

# Verify python install
RUN python3 -c "import sys; print(sys.version_info)" &&\
    python3 -c "import torch; print('torch version: ' + torch.__version__)" &&\
    python3 -c "from numpy import linalg as la; print('numpy.linalg/norm : ' + str(la.norm([1,2,3])))"

COPY thing.py /app

RUN python3 thing.py

COPY deps.edn /app

RUN clojure -P -M:test -M:build

COPY ./src /app/src
COPY ./src /app/resources

COPY ./tests.edn ./build.clj /app/
COPY ./test /app/test

RUN clojure -T:build uber

RUN clojure -T:build gen-code!

RUN clojure -M:jdk17:test


EXPOSE 3333

CMD [ "java", "--add-modules", "jdk.incubator.foreign", "--enable-native-access=ALL-UNNAMED", "-cp", "/app/target/cpb-standalone.jar", "clojure.main", "-m", "se.jobtechdev.cpb.core"]
