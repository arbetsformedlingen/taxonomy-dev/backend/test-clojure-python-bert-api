(ns build
  (:require [clojure.tools.build.api :as b]
            [libpython-clj2.python :as py]
            [libpython-clj2.require :as rp]
            [libpython-clj2.codegen :as codegen]))

(defn gen-code! [_]
  (py/initialize!)
  (rp/require-python '[sentence_transformers])
  (codegen/write-namespace!
   "builtins"
   {:output-dir "generated"
    :symbol-name-remaps {"AssertionError" "PyAssertionError"
                         "Exception" "PyException"}})
  (codegen/write-namespace!
   "sentence_transformers"
   {:output-dir "generated"
    :symbol-name-remaps {"AssertionError" "PyAssertionError"
                         "Exception" "PyException"}}))

#_(def lib 'se.jobtechdev/cpb)
#_(def version (format "1.2.%s" (b/git-count-revs nil)))
(def class-dir "target/classes")
(def basis (b/create-basis {:project "deps.edn"}))
(def uber-file "target/cpb-standalone.jar" #_(format "target/%s-%s-standalone.jar" (name lib) version))

(defn clean [_]
  (b/delete {:path "generated"})
  (b/delete {:path "classes"})
  (b/delete {:path "target"}))

(defn uber [_]
  (clean nil)
  (gen-code! nil)
  (b/copy-dir {:src-dirs ["src" "resources" "generated"]
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :java-opts ["--add-modules" "jdk.incubator.foreign"
                              "--enable-native-access=ALL-UNNAMED"]
                  :src-dirs ["src" "resources" "classes"]
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis
           :main 'se.jobtechdev.cpb.core}))