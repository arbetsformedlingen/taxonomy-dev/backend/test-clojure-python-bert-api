from sentence_transformers import SentenceTransformer

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")

print([sum(x*x) for x in model.encode(["Idag", "Imorgon", "Hästtjuvar"])])
