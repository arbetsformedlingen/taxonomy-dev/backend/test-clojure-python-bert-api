# Calling Python from Clojure

The primary purpose of this project was to act as a test system for learning how to launch things in OpenShift. The secondary purpose was to explore how to access functionality written in Python from Clojure.

Since the second purpose is more interesting, this documentation focuses on that.

## Getting started

Add the [clj-python/libpython-clj](https://github.com/clj-python/libpython-clj) dependency and require `libpython-clj2.python`. Run `libpython-clj2.python/initialize!` and if Python is found this should succeed. Now the default built in Python commands can be used.

### Using Python libraries (dynamic)

To use a library like `sentence_transformers` it first needs to be installed in the Python environment. When it is available there it can be imported into Clojure using the `require-python` function from the `libpython-clj2.require` namespace. This should make the entries in the Python library available in the current Clojure namespace.

### Using Python libraries (codegen)

The dynamic version creates interoperability code for a given Python library. To avoid this step it is possible to write the generated namespace to disk. This is accomplished by calling the `write-namespace!` function in the `libpython-clj2.codegen` namespace. The call requires the name of the Python library that one wants to store and it will use `python` as a default namespace root. So if we were to call `(libpython-clj2.codegen/write-namespace! "sentence_transformers")` we could then require `python.sentence_transformers` as we would any Clojure library.

Using Python this way still requires the `libpython-clj2.python/initialize!` call to enable the Python functions.

## Pitfalls

There were some difficulties in getting the system to run in REPL, terminal and containerised environment.

### JDK

If the JDK version used is fairly recent (JDK 17) the project needs to start with foreign modules and native access enabled using the flags `--add-modules jdk.incubator.foreign` and `--enable-native-access=ALL-UNNAMED`. (See [deps.edn](./deps.edn) `:alias :jdk17`.)

### Python

Getting Python to play nice with the Clojure project turned out to be tricky. The interoperability library was happiest with Python versions in the 3.8 - 3.9 range. The containerised version uses 3.8 and locally during development 3.9.15 was used.

The easiest way to deal with Python versions turned out to be [PyEnv](https://github.com/pyenv/pyenv), adding a version locally (see [.python-version](./.python-version)) and then installing all desired Python libraries globally for that install. Using virtual environments met with resistance.

### Containerisation

The system still requires write functionality in the container. There is further ahead of time compilation and code generation steps to explore here. Running `clojure -T:build gen-code!` should create the necessary Python namespaces and makes it possible to build an uber jar. The system crashes with 500 responses when running in a locked down environment though.

### Run for first time

If you run this project for first time after installed Python environment and library like `sentence_transformers` you should run `python thing.py`.