from transformers import pipeline

nlp = pipeline('ner', model='KB/bert-base-swedish-cased-ner', tokenizer='KB/bert-base-swedish-cased-ner')

# print(nlp)
# <transformers.pipelines.token_classification.TokenClassificationPipeline object at 0x7fe03beb41f0>

# ner -> token-classification
# TokenClassificationPipeline, AutoModelForTokenClassification
# type -> text

print(nlp('Jag hade en gång en båt.'))

